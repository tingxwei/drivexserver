const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
var Pusher = require('pusher');
const shortId = require('shortid')
const app = express();
const session = require('express-session')
const PORT =  process.env.PORT || 3000; //process.argv[2]
const admin = require("firebase-admin");
const keys = require('')
require('dotenv').config()
admin.initializeApp({
    credential: admin.credential.cert(keys),
    databaseURL: "https://drivex-61d6c.firebaseio.com"
});
var MemoryStore = require('memorystore')(session)


app.use(cors({
    credentials: true,
    origin: [undefined, 'http://localhost:4200', 'http://localhost:4200', 'http://localhost',
        'http://192.168.1.88', 'http://192.168.1.88:4200']
}));
app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

var pusher = new Pusher({
    appId: '599171',
    key: 'b50bab0c677dbe48e1d9',
    secret: '25b14cf7e27fdc31fff8',
    cluster: 'ap1',
    encrypted: true
  });

  app.post('/message', async (req, res) => {
    // simulate actual db save with id and createdAt added
    const chat = {
      ...req.body,
      id: shortId.generate(),
      createdAt: new Date().toISOString()
    } 
    // trigger this update to our pushers listeners
    pusher.trigger('chat-group', 'chat', chat)
    res.send(chat)
  })

  app.post('/join', (req, res) => {
    const chat = {
      ...req.body,
      id: shortId.generate(),
      type: 'joined',
      createdAt: new Date().toISOString()
    }
    // trigger this update to our pushers listeners
    pusher.trigger('chat-group', 'chat', chat)
    res.send(chat)
  })



function isAuthenticate(req, res, next) {
    if (req.headers.authorization != null) {
        console.log(req.headers.authorization);
        var authIdToken = req.headers.authorization.split(' ')[1];
        console.log("authIdToken" + authIdToken);
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Token' ||
            req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            admin.auth().verifyIdToken(req.headers.authorization.split(' ')[1]).then(function (result) {
                if (result != null) {
                    req.session.firebaseEmail = result.email;
                    return next();
                } {
                    throw new Error('Firebase unable to verify');
                }
            }).catch(error => {
                console.log(error);
                return res.status(403).json({ error: 'Access Denied with firebase verification error' });
            });
        } else {
            return res.status(403).json({ error: 'Access Denied' });
        }
    } else {
        return res.status(403).json({ error: 'Access Denied' });
    }
}

app.use(session({
    store: new MemoryStore({
        checkPeriod: 86400000 // prune expired entries every 24h
      }),
    secret: 'thisisasecret',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 365 * 24 * 60 * 60 * 1000 }
}))


app.post('/api/addtrip', isAuthenticate, (req, res) => {
    console.log(req.body);
    firebaseEmail = req.session.firebaseEmail;
    admin.database().ref('trips').push({
        day: req.body.day,
        month: req.body.month,
        year: req.body.year,
        dayOfTheWeek: req.body.dayOfTheWeek,
        pickUpTime: req.body.pickUpTime,
        pickUpTime2: req.body.pickUpTime,
        pickUpLocation: req.body.pickUpLocation,
        dropOffTime: req.body.dropOffTime,
        dropOffLocation: req.body.dropOffLocation,
        price: req.body.price,
        status: req.body.status,
        remarks: req.body.remarks,
        email: firebaseEmail,
        lat: req.body.lat,
        lng: req.body.lng,
    })
        .then((result) => {
            res.status(200);
            res.json(result)
        })
})

function snapshotToArray(snapshot) {
    var returnArr = [];
    snapshot.forEach(function (childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;
        returnArr.push(item);
    });
    return returnArr;
};



app.get('/api/getTrips', (req, res) => {
    var email = req.session.firebaseEmail
    console.log('Firebase Endpoint')
    var getTripRef = admin.database().ref('trips');
    getTripRef.orderByChild('email').equalTo(email).limitToLast(1000).once('value')
        .then((snapshot) => {
            var value = snapshotToArray(snapshot);
            console.log(snapshotToArray(snapshot));
            if (value) {
                res.status(200).json(value);
            } else {
                res.status(401).json({ error: 'No trips found' })
            }
        })

})

app.get('/api/search', isAuthenticate, (req, res) => {
   var dotw = req.query.day;
    console.log('Firebase Endpoint')
    var getTripRef = admin.database().ref('trips');
    getTripRef.orderByChild('dayOfTheWeek').equalTo(dotw).limitToLast(1000).once('value')
        .then((snapshot) => {
            var value = snapshotToArray(snapshot);
            console.log(snapshotToArray(snapshot));
            if (value) {
                res.status(200).json(value);
            } else {
                res.status(401).json({ error: 'No trips found' })
            }
        })

})

app.post('/api/deleteTrip', isAuthenticate, (req, res) => {
    console.log('lala')
    console.log(req.body);
    admin.database().ref('trips').child(req.body.tripK).remove()
        .then((result)=> {
            res.status(200);
            res.json({ Result: 'failed' })
        })
        .catch((error)=> {
            res.status(400);
            res.json({ Result: 'success' })
        });

})


app.post('/savestatus', isAuthenticate, (req, res) => {
    console.log(req.body.status);
    firebaseEmail = req.session.firebaseEmail;
    lastUpdate = new Date();
    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    formattedDate = lastUpdate.toLocaleDateString("en-US", options)
    admin.database().ref('users').push({
        user: firebaseEmail,
        paid_status: req.body.status,
        last_update: formattedDate,
    })
        .then((result) => {
            res.status(200);
            res.json(result);
        })
})



app.use(express.static(__dirname + '/public'));


app.listen(PORT, () => {
    console.log('Starting application on port %d', PORT);
})
